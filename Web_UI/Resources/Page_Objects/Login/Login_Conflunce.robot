*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported SeleniumLibrary.
Library           SeleniumLibrary
Library           String
Resource          ./Login_Conflunce_PO.robot
*** Keywords ***
Navigate To Netlink Confluence Login Page
    Open Browser    ${CONF_URL}    ${BROWSER}
    Maximize Browser Window
    Confluence Login Page Should Be Open

Confluence Login Page Should Be Open
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  Wait Until Page Contains     Log in
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  Wait Until Page Contains     Confluence
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  Title Should Be    Log In - Confluence
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  Wait Until Page Contains     Forgot your password?

Login To Netlink Confluence Portal
    Enter Confluence UserName
    Enter Confluence Password
    Click to confluence Login

Validate Netlink Confluence Dashboard Defaults
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  Wait Until Page Contains     Discover
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  Wait Until Page Contains     My Work
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  Wait Until Page Contains     Recently worked on
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  Wait Until Page Contains     Recently visited
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  Wait Until Page Contains     Saved for later
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  Wait Until Page Contains     Spaces
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  Wait Until Page Contains     People
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  Wait Until Page Contains     All updates
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  Wait Until Page Contains     Popular


Logout From Netlink Confluence Portal
    Click to Logout button
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  Wait Until Page Contains     You have been successfully logged out.





