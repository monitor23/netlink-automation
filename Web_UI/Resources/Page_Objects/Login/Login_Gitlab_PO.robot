*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported SeleniumLibrary.
Library           SeleniumLibrary
Library           String

*** Keywords ***
Enter gitlab UserName
    [Arguments]    ${username}=${GITLAB_USERNAME}
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}   seleniumlibrary.input text    //*[@id="user_login"]    ${username}

Enter gitlab Password
    [Arguments]    ${password}=${GITLAB_PASSWORD}
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}   seleniumlibrary.input text    //*[@id="user_password"]    ${password}

Click to gitlab Login
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}   seleniumlibrary.click element     //*[@id="new_user"]/div[5]/input

Validate login success
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  title should be   Projects · Dashboard · GitLab

Check For Invalid User name and Password
    ${message}  wait until keyword succeeds  ${TIMEOUT}  ${RETRY}   seleniumlibrary.page should contain     Invalid Login or password.

Click to gitlab Register link
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  seleniumlibrary.click element     //*[@id="signin-container"]/ul/li[2]/a

Validate the Gitlab Register Page Loaded
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  page should contain   Register
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  page should contain   Full name
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  page should contain   Password
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  page should contain   Email
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  page should contain   Email confirmation
    ${id}=	  Get Element Attribute	  //*[@id="new_new_user"]/div[8]/input	  value
    should be equal as strings   ${id}   Register