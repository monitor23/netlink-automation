*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported SeleniumLibrary.
Library           SeleniumLibrary
Library           String

*** Keywords ***
Enter Testlink UserName
    [Arguments]    ${username}=${TESTLINK_USERNAME}
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}   seleniumlibrary.input text    //*[@id="tl_login"]    ${username}

Enter Testlink Password
    [Arguments]    ${password}=${TESTLINK_PASSWORD}
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}   seleniumlibrary.input text    //*[@id="tl_password"]    ${password}

Click to Testlink Login
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}   seleniumlibrary.click element     //*[@id="tl_login_button"]

Check For Invalid User name and Password
    ${message}  wait until keyword succeeds  ${TIMEOUT}  ${RETRY}   seleniumlibrary.page should contain     Wrong password. Reset it?

Validate Testlink Success Login
    ${title}    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  get title
    should contain   ${title}    TestLink
    unselect frame
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  select frame    	//iframe[@name='mainframe']
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  page should contain   Test Plan Management
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  page should contain   Issue Tracker Management
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  page should contain   Code Tracker Management
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  page should contain   Test Project Management
    #unselect frame

Click TL Logout option
    unselect frame
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  select frame    //iframe[@name='titlebar']
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}   seleniumlibrary.mouse over        //*[@title='Logout']
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}   seleniumlibrary.click element     //*[@title='Logout']
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  page should contain    Login
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  page should contain    New User
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  page should contain    Lost Password
    unselect frame