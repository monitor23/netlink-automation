*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported SeleniumLibrary.
Library           SeleniumLibrary
Library           String
Resource          ./Login_Gitlab_PO.robot
*** Keywords ***
Navigate To Netlink Gitlab Login Page
    Open Browser    ${GITLAB_URL}    ${BROWSER}    options=add_argument("--disable-popup-blocking"); add_argument("--ignore-certificate-errors")
    Maximize Browser Window

Gitlab Login Page Should Be Open
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  SeleniumLibrary.Title Should Be     Sign in · GitLab
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  page should contain     GitLab Community Edition
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  page should contain     A complete DevOps platform
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  page should contain     Sign in
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  page should contain     Username or email
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  page should contain     Password
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  page should contain     Remember me
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  page should contain     Forgot your password?

Login To Netlink Gitlab Portal Invalid
    Enter gitlab UserName    test
    Enter gitlab Password    test123
    Click to gitlab Login
    Check For Invalid User name and Password

Login To Netlink Gitlab Portal
    Enter gitlab UserName
    Enter gitlab Password
    Click to gitlab Login

Register Page Navigation Gitlab Netlink Portal
    Click to gitlab Register link
    Validate the Gitlab Register Page Loaded
    sleep    3s