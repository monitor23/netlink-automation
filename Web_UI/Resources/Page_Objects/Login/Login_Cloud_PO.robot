*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported SeleniumLibrary.
Library           SeleniumLibrary
Library           String

*** Keywords ***
Enter cloud UserName
    [Arguments]    ${username}=${CLOUD_USERNAME}
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}   seleniumlibrary.input text    //*[@id="user"]    ${username}

Enter cloud Password
    [Arguments]    ${password}=${CLOUD_PASSWORD}
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}   seleniumlibrary.input text    //*[@id="password"]    ${password}

Click to cloud Login
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}   seleniumlibrary.click element     //*[@id="submit"]

Check For Invalid User name and Password
    ${message}  wait until keyword succeeds  ${TIMEOUT}  ${RETRY}   seleniumlibrary.page should contain     Wrong password. Reset it?

Validate Cloud Success Login
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  SeleniumLibrary.Title Should Be     Files - ownCloud
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  page should contain   edgar
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  page should contain   Name
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  page should contain   Size
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  page should contain   Modified

Click Cloud Logout option
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}   seleniumlibrary.click element     //*[@id="expandDisplayName"]
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}   seleniumlibrary.click element     //*[@id="logout"]
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}   seleniumlibrary.page should contain    A safe home for all your data
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}   seleniumlibrary.page should contain    ownCloud