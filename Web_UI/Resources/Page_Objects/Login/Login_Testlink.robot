*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported SeleniumLibrary.
Library           SeleniumLibrary
Library           String
Resource          ./Login_Testlink_PO.robot
*** Keywords ***
Navigate To Netlink Testlink Login Page
    Open Browser    ${TESTLINK_URL}    ${BROWSER}    options=add_argument("--disable-popup-blocking"); add_argument("--ignore-certificate-errors")
    Maximize Browser Window

Testlink Login Page Should Be Open
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  SeleniumLibrary.Title Should Be     Login
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  page should contain    Login
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  page should contain    New User
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  page should contain    Lost Password

Login To Netlink Testlink Portal Valid
    Enter Testlink UserName
    Enter Testlink Password
    Click to Testlink Login

Logout testlink portal
    Click TL Logout option

Login To Netlink Testlink Portal invalid
    Enter Testlink UserName     invalid
    Enter Testlink Password     invalid
    Click to Testlink Login

Validate Invalid Testlink login
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  page should contain    Try again! Wrong login name or password!