*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported SeleniumLibrary.
Library           SeleniumLibrary
Library           String

*** Keywords ***
Enter Redmine UserName
    [Arguments]    ${username}=${CONF_USERNAME}
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}   seleniumlibrary.input text    //*[@id="username"]    ${username}

Enter Redmine Password
    [Arguments]    ${password}=${CONF_PASSWORD}
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}   seleniumlibrary.input text    //*[@id="password"]    ${password}

Click to Redmine Login
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}   seleniumlibrary.click element     //*[@id="login-submit"]

Check For Invalid User name and Password
    ${message}  wait until keyword succeeds  ${TIMEOUT}  ${RETRY}   seleniumlibrary.get text     //*[@id="flash_error"]
    should be equal as strings   ${message}   Invalid user or password

Click to Register link
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  seleniumlibrary.click element     //*[@id="account"]/ul/li[2]/a

Validate the Register Page Loaded
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  page should contain   Register
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  page should contain   Login
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  page should contain   Password
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  page should contain   Confirmation
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  page should contain   First name
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  page should contain   Last name
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  page should contain   Email
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  page should contain   Hide my email address
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  page should contain   Language
    ${id}=	  Get Element Attribute	  //*[@id="new_user"]/input[3]	  value
    should be equal as strings   ${id}   Submit