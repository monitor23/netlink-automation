*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported SeleniumLibrary.
Library           SeleniumLibrary
Library           String

*** Keywords ***
Enter Confluence UserName
    [Arguments]    ${username}=${CONF_USERNAME}
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}   seleniumlibrary.input text    os_username    ${username}

Enter Confluence Password
    [Arguments]    ${password}=${CONF_PASSWORD}
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}   seleniumlibrary.input text    os_password    ${password}

Click to confluence Login
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}   seleniumlibrary.click element     //*[@id="loginButton"]

Click to Logout button
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}   seleniumlibrary.click element     //*[@id="user-menu-link"]/div/div/img
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}   seleniumlibrary.click element     //*[@id="logout-link"]