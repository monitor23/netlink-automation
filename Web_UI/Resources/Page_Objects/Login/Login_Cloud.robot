*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported SeleniumLibrary.
Library           SeleniumLibrary
Library           String
Resource          ./Login_Cloud_PO.robot
*** Keywords ***
Navigate To Netlink Cloud Login Page
    Open Browser    ${CLOUD_URL}    ${BROWSER}    options=add_argument("--disable-popup-blocking"); add_argument("--ignore-certificate-errors")
    Maximize Browser Window

Cloud Login Page Should Be Open
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  SeleniumLibrary.Title Should Be     ownCloud

Login To Netlink Cloud Portal Invalid
    Enter cloud UserName    test
    Enter cloud Password    test123
    Click to cloud Login
    Check For Invalid User name and Password

Login To Netlink Cloud Portal Valid
    Enter cloud UserName
    Enter cloud Password
    Click to cloud Login

Logout Cloud portal
    Click Cloud Logout option