*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported SeleniumLibrary.
Library           SeleniumLibrary
Library           String
Resource          ./Login_Redmine_PO.robot
*** Keywords ***
Navigate To Netlink Redmine Login Page
    Open Browser    ${REDMINE_URL}    ${BROWSER}    options=add_argument("--disable-popup-blocking"); add_argument("--ignore-certificate-errors")
    Maximize Browser Window

Redmine Login Page Should Be Open
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  Wait Until Page Contains     Login
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  Wait Until Page Contains     Lost password
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  Wait Until Page Contains     Password
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  element should be enabled     //*[@id="login-submit"]
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  element should be enabled     //*[@id="username"]
    wait until keyword succeeds  ${TIMEOUT}  ${RETRY}  element should be enabled     //*[@id="password"]
    sleep   3s

Login To Netlink Redmine Portal Invalid
    Enter Redmine UserName    test
    Enter Redmine Password    test123
    Click to Redmine Login
    Check For Invalid User name and Password

Register Page Navigation Redmine Netlink Portal
    Click to Register link
    Validate the Register Page Loaded
    sleep    3s