*** Settings ***
Documentation     A test suite Testlink Portal scenarios
Resource          ../Resources/Page_Objects/Login/Login_testlink.robot

*** Test Cases ***
TS001-Netlink Testlink Portal Valid Login and Logout
    [Documentation]  This test is for the Verification of the Cloud portal for Smoke test
    [Tags]      Netlink   Testlink   Smoke   Negative
    Navigate To Netlink Testlink Login Page
    Testlink Login Page Should Be Open
    Login To Netlink Testlink Portal valid
    Validate Testlink Success Login
    logout testlink portal
    [Teardown]   Close All Browsers

TS002-Netlink Testlink Portal Invalid Login
    [Documentation]  This test is for the Verification of the Cloud portal for Smoke test
    [Tags]      Netlink   Testlink   Smoke   Negative
    Navigate To Netlink Testlink Login Page
    Testlink Login Page Should Be Open
    Login To Netlink Testlink Portal invalid
    Validate Invalid Testlink login
    [Teardown]   Close All Browsers