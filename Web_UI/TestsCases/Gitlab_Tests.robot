*** Settings ***
Documentation     A test suite Redmine Portal scenarios
Resource          ../Resources/Page_Objects/Login/Login_Gitlab.robot

*** Test Cases ***
TS001-Verify that Netlink Gitlab Portal Is Working
    [Documentation]  This test is for the Verification of the Confluence portal for Smoke test
    [Tags]      Netlink   Gitlab   Smoke   Negative
    Navigate To Netlink Gitlab Login Page
    Gitlab Login Page Should Be Open
    Login To Netlink Gitlab Portal Invalid
    Register Page Navigation Gitlab Netlink Portal
    [Teardown]   Close All Browsers

TS002-Verify that Netlink Redmine Portal Is Working
    [Documentation]  This test is for the Verification of the Confluence portal for Smoke test
    [Tags]      Netlink   Gitlab   Smoke   Positive
    Navigate To Netlink Gitlab Login Page
    Gitlab Login Page Should Be Open
    Login To Netlink Gitlab Portal
    Validate login success
    [Teardown]   Close All Browsers
