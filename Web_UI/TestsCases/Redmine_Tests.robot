*** Settings ***
Documentation     A test suite Redmine Portal scenarios
Resource          ../Resources/Page_Objects/Login/Login_Redmine.robot

*** Test Cases ***
TS001-Verify that Netlink Redmine Portal Is Working
    [Documentation]  This test is for the Verification of the Confluence portal for Smoke test
    [Tags]      Netlink   Redmine   Smoke   Positive
    Navigate To Netlink Redmine Login Page
    Redmine Login Page Should Be Open
    Login To Netlink Redmine Portal Invalid
    Register Page Navigation Redmine Netlink Portal
    [Teardown]   Close All Browsers
