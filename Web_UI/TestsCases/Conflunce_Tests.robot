*** Settings ***
Documentation     A test suite with a Fin Message Template Scenario
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.
Resource          ../Resources/Page_Objects/Login/Login_Conflunce.robot


*** Test Cases ***
TS001-Verify that Netlink Confluence Portal Is Working
    [Documentation]  This test is for the Verification of the Confluence portal for Smoke test
    [Tags]      Netlink   Confluence   Smoke   Positive
    Navigate To Netlink Confluence Login Page
    Login To Netlink Confluence Portal
    Validate Netlink Confluence Dashboard Defaults
    Logout From Netlink Confluence Portal
    [Teardown]   Close All Browsers
