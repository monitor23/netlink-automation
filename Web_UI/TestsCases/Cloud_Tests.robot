*** Settings ***
Documentation     A test suite Cloud Portal scenarios
Resource          ../Resources/Page_Objects/Login/Login_cloud.robot

*** Test Cases ***
TS001-Netlink Cloud portal Invalid Login
    [Documentation]  This test is for the Verification of the Cloud portal for Smoke test
    [Tags]      Netlink   Cloud   Smoke   Negative
    Navigate To Netlink Cloud Login Page
    Cloud Login Page Should Be Open
    Login To Netlink Cloud Portal Invalid
    [Teardown]   Close All Browsers

TS002-Netlink Cloud Portal Valid Login and Logout
    [Documentation]  This test is for the Verification of the Cloud portal for Smoke test
    [Tags]      Netlink   Cloud   Smoke   Negative
    Navigate To Netlink Cloud Login Page
    Cloud Login Page Should Be Open
    Login To Netlink Cloud Portal valid
    Logout Cloud portal
    [Teardown]   Close All Browsers
